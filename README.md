# README #

This README would normally document whatever steps are necessary to get your application up and running.

#What is this repository for? 

This is a REST service, built with Spring Boot, to allow end-users to pull
station data.

Station Id            - Integer,
Station Name          - String,
Is station HD enabled - boolean,
Call Sign             - String


This Spring Boot application that meets the following criteria

# Includes the following dependencies
a. Gradle - Build tool
b. Swagger - (UI documentation Tool)
c. Jacoco  - (Code Coverage)
d. Junit   - Software testing tool
e. Accuator
f. H2 Database
g. MyBatis  - (ORM)

# Functionality

a. User must be able to access API endpoints and retrieve data
b. User must be able to add a station
c. User must be able to remove a station
d. User must be able to update a station
e. User must be able to search by stations by station ID or name
f. User must be able to search HD enabled stations
