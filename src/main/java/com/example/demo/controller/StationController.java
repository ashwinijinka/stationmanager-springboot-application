package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Station;
import com.example.demo.service.StationService;
/**
 * Controller layer for operations on Station entity
 **/
@RestController
@RequestMapping(value = "/stations")
public class StationController {

	@Autowired
	StationService stationService;

	/**
	 * Get all the stations from the database
	 **/
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Station> getAllStations() {
		return stationService.getAllStations();
	}
	/**
	 * Get the station with specific id
	 **/
	@RequestMapping(method = RequestMethod.GET, value = "/id/{id}")
	public ResponseEntity<HashMap<String, Object>> getStationById(@PathVariable int id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("data", stationService.getStationById(id));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	/**
	 * Get all the stations with specific name
	 **/

	@RequestMapping(method = RequestMethod.GET, value = "/name/{name}")
	public List<Station> getStationByName(@PathVariable String name) {
		return stationService.getStationByName(name);
	}
	/**
	 * Get all the hdEnabled stations
	 **/


	@RequestMapping(method = RequestMethod.GET, value = "/hdEnabled")
	public ResponseEntity<HashMap<String, Object>> getHdEnabledStation() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("data", stationService.getHdEnabledStation());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	/**
	 * Add a stations to the database
	 **/
	@RequestMapping(method = RequestMethod.POST)
	public void addStation(@RequestBody Station station) {
		stationService.addStation(station);
	}
	/**
	 * Update a station in the database
	 **/
	@RequestMapping(method = RequestMethod.PUT)
	public void updateStation(@RequestBody Station station) {
		stationService.updateStation(station);
	}
	/**
	 * Delete the stations with specific id
	 **/

	@RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
	public void deleteStation(@PathVariable int id) {
		stationService.deleteStation(id);
	}
}