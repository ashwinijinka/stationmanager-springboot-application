package com.example.demo.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.example.demo.entity.Station;

/**
 *Mapper interface to map methods to database operations in StationMapper.xml
 **/

@Mapper
public interface StationMapper {
	/**
	 * Get all the stations from the database
	 **/
	public List<Station> getAllStations();
	/**
	 * Get the station with specific id
	 **/
	public Station getStationById(int id);
	/**
	 * Get all the stations with specific name
	 **/

	public List<Station> getStationByName(String name);
	/**
	 * Get all the hdEnabled stations
	 **/
	public List<Station> getHdEnabledStation();
	/**
	 * Add a stations to the database
	 **/
	public int addStation(Station station);
	/**
	 * Update a station in the database
	 **/
	public int updateStation(Station station);
	/**
	 * Delete the stations with specific id
	 **/
	public int deleteStation(int id);
}

