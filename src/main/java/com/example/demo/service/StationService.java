package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Station;
import com.example.demo.mapper.StationMapper;


/**
 * Service layer representation of operation on Station entity
 **/

@Service
public class StationService {

	@Autowired
	StationMapper stationMapper;

	/**
	 * Get all the stations from the database
	 **/

	public List<Station> getAllStations() {
		return stationMapper.getAllStations();
	};

	/**
	 * Get the station with specific id
	 **/

	public Station getStationById(int id) {
		return stationMapper.getStationById(id);
	};

	/**
	 * Get all the stations with specific name
	 **/

	public List<Station> getStationByName(String name) {
		return stationMapper.getStationByName(name);
	};

	/**
	 * Get all the hdEnabled stations
	 **/

	public List<Station> getHdEnabledStation() {
		return stationMapper.getHdEnabledStation();
	};

	/**
	 * Add a stations to the database
	 **/

	public int addStation(Station station) {
		return stationMapper.addStation(station);
	};

	/**
	 * Update a station in the database
	 **/

	public int updateStation(Station station) {
		return stationMapper.updateStation(station);
	}

	/**
	 * Delete the stations with specific id
	 **/

	public int deleteStation(int id) {
		return stationMapper.deleteStation(id);
	};
}