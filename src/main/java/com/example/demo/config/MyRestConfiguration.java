package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import com.example.demo.entity.Station;


@Configuration
public class MyRestConfiguration extends RepositoryRestConfigurerAdapter {

	/**
	 * Expose id values in REST api reponse
	 **/
@Override
public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    config.exposeIdsFor(Station.class);
}
}