package com.example.demo.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Station entity class 
 **/


@ApiModel

public class Station implements Serializable {


	private static final long serialVersionUID = -7269229713811070577L;
	 @ApiModelProperty(position = 1, required = true, value = "1")
	private int id;
	private String name;
	private String callSign;
	private boolean hdEnabled;

	public Station() {
	}

	public Station(int id, String name, String callSign, boolean hdEnabled) {
		this.id = id;
		this.name = name;
		this.callSign = callSign;
		this.hdEnabled = hdEnabled;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHdEnabled() {
		return hdEnabled;
	}

	public void setHdEnabled(boolean hdEnabled) {
		this.hdEnabled = hdEnabled;
	}

	public String getCallSign() {
		return callSign;
	}

	public void setCallSign(String callSign) {
		this.callSign = callSign;
	}

}
