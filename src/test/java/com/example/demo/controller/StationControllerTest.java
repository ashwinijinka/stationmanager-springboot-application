package com.example.demo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.AbstractControllerTest;
import com.example.demo.entity.Station;
import com.example.demo.service.StationService;
/**
 * jUnit test cases for controller methods
 **/

@Transactional

public class StationControllerTest extends AbstractControllerTest {

	@Autowired
	private StationService stationservice;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testGetAllStationsController() throws Exception {
		String uri = "/stations";

		MvcResult result = mvc.perform(get(uri)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andReturn();

		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("failure - expected HTTP status 200", 200, status);
		Assert.assertTrue("failure - expected HTTP response body to have a value", content.trim().length() > 0);
	}

	@Test
	public void testGetStationByIdController() throws Exception {

		String uri = "/stations/id/{id}";
		int id = 101;

		MvcResult result = mvc.perform(get(uri, id)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andReturn();

		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("failure - expected HTTP status 200", 200, status);
		Assert.assertTrue("failure - expected HTTP response body to have a value", content.trim().length() > 0);

	}

	@Test
	public void testGetHdEnabledStationController() throws Exception {

		String uri = "/stations/hdEnabled";

		MvcResult result = mvc.perform(get(uri)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andReturn();

		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("failure - expected HTTP status 200", 200, status);
		Assert.assertTrue("failure - expected HTTP response body to have a value", content.trim().length() > 0);

	}

	@Test
	public void testGetStationByNameController() throws Exception {

		String uri = "/stations/name/{name}";
		String name = "Radio Two";

		MvcResult result = mvc.perform(get(uri, name)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8")).andReturn();

		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("failure - expected HTTP status 200", 200, status);
		Assert.assertTrue("failure - expected HTTP response body to have a value", content.trim().length() > 0);

	}

	@Test
	public void testAddStationController() throws Exception {

		String uri = "/stations";
		Station station = new Station(111, "Radio 11", "ww11", true);
		String inJson = super.mapToJson(station);

		mvc.perform(
				post(uri).accept(MediaType.APPLICATION_JSON).content(inJson).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(HttpStatus.CREATED.value())).andReturn();

	}

	@Test
	public void testUpdateStationController() throws Exception {

		String uri = "/stations";
		Station station = new Station(101, "Radio updated", "ww11", true);
		String inJson = super.mapToJson(station);

		mvc.perform(put(uri).accept(MediaType.APPLICATION_JSON).content(inJson).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void testDeleteStation() throws Exception {

		String uri = "/stations/id/{id}";
		int id = 105;

		MvcResult result = mvc.perform(delete(uri, id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(204)).andReturn();

		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("failure - expected HTTP status 204", 204, status);
		Assert.assertTrue("failure - expected HTTP response body to be empty", content.trim().length() == 0);

	}

}
