package com.example.demo.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.AbstractTest;
import com.example.demo.entity.Station;
/**
 * jUnit test cases for service methods
 **/
@Transactional
public class StationServiceTest extends AbstractTest {

	@Autowired
	private StationService stationService;

	@Test
	public void getAllStations() {
		List<Station> stations = stationService.getAllStations();
		Assert.assertNotNull("failure - expected not null", stations);
	}

	@Test
	public void getStationById() {

		int id = 101;

		Station station = stationService.getStationById(id);
		Assert.assertNotNull("failure - expected not null", station);
		Assert.assertEquals("failure - expected id attribute match", id, station.getId());

	}

	@Test
	public void getStationByName() {

		String name = "Radio One";

		List<Station> stations = stationService.getStationByName(name);

		Assert.assertNotNull("failure - expected not null", stations);
		stations.forEach(s -> {
			Assert.assertEquals("failure - expected name attribute match", name, s.getName());
		});
	}
	@Test
	public void getStationByHdEnabled() {

		List<Station> stations = stationService.getHdEnabledStation();
		Assert.assertNotNull("failure - expected not null", stations);
		stations.forEach(s -> {
			Assert.assertEquals("failure - expected hd attribute match", true, s.isHdEnabled());
		});
	}
	@Test
	public void testAddStation() {
		int id = 110;
		Station station = new Station(id, "Radio 10", "ww10", true);
		stationService.addStation(station);
		Station newStation = stationService.getStationById(id);

		Assert.assertNotNull("failure - expected not null", newStation);
		Assert.assertNotNull("failure - expected id attribute not null", newStation.getId());
		Assert.assertEquals("failure - expected text attribute match", id, newStation.getId());
	}
	
	 @Test public void testUpdateStation() {
		 int id = 101;
		 String updatedName = "Radio One updated";
		 String updatedCallSign = "ww2";
		 boolean updatedHd = true;
		
		 Station station = new Station(id,updatedName,updatedCallSign,updatedHd);
		 stationService.updateStation(station);
		 Station updatedStation = stationService.getStationById(id);
		 Assert.assertNotNull("failure - expected not null", updatedStation);
		 Assert.assertEquals("failure - expected name attribute match", updatedName,
				 updatedStation.getName());
		 Assert.assertEquals("failure - expected callSign attribute match", updatedCallSign,
		         updatedStation.getCallSign());
		 Assert.assertEquals("failure - expected hd attribute match", updatedHd,
		         updatedStation.isHdEnabled());
		 
		 }
	 
	 	@Test public void testDeleteStation() {
		 int id = 101;
		 stationService.deleteStation(id);
		 Assert.assertNull("failure - expected null", stationService.getStationById(id));
		 
	 	}
}
