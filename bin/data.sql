INSERT
INTO	station
(	station_stationId,	
        station_name,
        station_callSign,
	station_hdEnabled
)
VALUES
(101,'Radio One','WW1',FALSE),
(102,'Radio Two','WW2',TRUE),
(103,'Radio Three','WW3',FALSE),
(104,'Radio Four','WW4',TRUE),
(105,'Radio Five','WW5',FALSE);