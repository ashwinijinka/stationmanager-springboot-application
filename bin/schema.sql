CREATE TABLE station(station_stationId INT PRIMARY KEY AUTO_INCREMENT,
                     station_name VARCHAR(100),
		     station_callSign VARCHAR(100),
		     station_hdEnabled BOOLEAN);